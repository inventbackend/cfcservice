﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Manager
{
    public class StringFacade
    {
        public static string NormalizedBranch(string branches)
        {

            if (branches.Contains('|'))
            {


                var sb = new StringBuilder();
                string[] arrBranch = branches.Split('|');
                if (arrBranch.Count() == 1)
                {
                    return arrBranch.FirstOrDefault().Trim();
                }
                else if (arrBranch.Count() > 1)
                {
                    string last = arrBranch[arrBranch.Count() - 1];
                    foreach (string str in arrBranch)
                    {
                        if (str == last)
                        {
                            sb.Append("'" + str + "'");
                        }
                        else
                        {
                            sb.Append("'" + str + "',");
                        }
                    }

                    return sb.ToString();
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "'" + branches + "'";
            }

        }
    }
}
